import pyautogui
import os
import time
import yaml
from yaml.loader import SafeLoader

with open('account.yaml') as account_file:
    account = yaml.load(account_file, Loader=SafeLoader)
    print(account)

os.system("setxkbmap -layout gb")
os.system("runelite")
time.sleep(10)

pyautogui.press('enter')
time.sleep(5)
pyautogui.write(account['email'], interval=0.1)
time.sleep(1)
pyautogui.press('enter')
pyautogui.write(account['password'], interval=0.1)
time.sleep(1)
pyautogui.press('enter')
