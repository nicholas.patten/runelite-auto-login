# Runelite Auto-Login

Auto Login script for Runelite launcher.

Created this script after I realised that I was unable to paste into the launcher.

## Dependencies

### Setup Virtual Environment

We don't want to pollute our OS python with project specific packages so first we're going to create a virtualenv.

1. Install the python 3 virtualenv package
    - `pip3 install virtualenv`
1. Create the virtualenv
    - `python3 -m virtualenv venv`

Now our virtualenv has been created we need to active it to install packages.

`source venv/bin/activate`

Activating the environment, means that any python script we execute will execute using this python environment rather than the OS/ System python install. It also means that any packages we install will be installed into this environment.

### Install Python Dependencies

Assuming you have followed the 'Setup Virtual Environment' steps and have activated your virtualenv, we can now install the required python dependencies. There is a file called `requirements.txt` that defines the python packages we require and the versions of those packages.

To install the packages, we can use pip:
`pip3 install -r requirements.txt`

Do make sure to install the TK package, before running the script, instructions in 'TK Package' section.

### TK Package

On Linux you require the TK package to due to the MouseInfo python dependency

Ubuntu: `sudo apt-get install python3-tk python3-dev`
Arch Linux: `sudo pacman -S tk` 

### Runelite Launcher

Rather obviously, you also need to have the Runelite launcher itself installed.

## Configure Account Details

To get the script to pick up our account details, we have to create an account.yaml file.

The `account.yaml` file requires two keys to be present:
- email
- password

If email or password is missing from the `account.yaml` file, the script will fail.

Within the repo there is a template for the account.yaml file called `account.yaml.template`.
You can copy this file, rename it to `account.yaml` and then fill in the appropriate values.

`cp account.yaml.template account.yaml`

## Usage

1. Activate your virtualenv
    - `source venv/bin/activate`
1. Run python script
    - `python3 main.py`
